# README #

This is java coding test for MAG, the maze runner application written in java.

### What is this repository for? ###

* The Maze Runner is java application and serve as a guidline between Brain and Runner
* 1.0-SNAPSHOT

### How do I get set up? ###

* This is a maven project (Maven 3.1.1, Java version 1.8.0_45)
* mvn clean install
* mvn exec:java -Dexec.mainClass="com.mag.mazerunner.Main"

### How to run? ###
* java -jar maze-server-1.0.jar
* mvn exec:java -Dexec.mainClass="com.mag.mazerunner.Main"

### Project structure ###
* com.mag.mazerunner.Main
* com.mag.mazerunner.Brain
* com.mag.mazerunner.Runner

### Contribution guidelines ###

* There are three test classes, using JUnit and Mockito

### Who do I talk to? ###
The Brain and Runner are talking via ArrayBlockingQueues, one for each direction. Brain is sending next coordinates and Runner is replying with move result.

Initial domain model counted with searching ideal way via recursive algorithm, that's why the Maze class contains several method as a validateMove(), getField(), move().
But finally I just reused the path exposed by endpoint.

Another disadvantage is, that I didn't use Spring IoC as I would wish, that's why autowiring of MazeService in Brain is done manually (passing as a parameter because of unit testing).
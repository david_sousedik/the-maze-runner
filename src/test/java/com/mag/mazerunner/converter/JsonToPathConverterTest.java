package com.mag.mazerunner.converter;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

import com.mag.mazerunner.domain.Maze;
import com.mag.mazerunner.domain.Path;

public class JsonToPathConverterTest {
    
    private LinkedHashMap<String, Integer> coordinates1;
    private LinkedHashMap<String, Integer> coordinates2;
    private LinkedHashMap<String, Integer> coordinates3;
    private List<LinkedHashMap<String, Integer>> coordinates;
    
    private JsonToPathConverter converter = new JsonToPathConverter();
    
    @Before
    public void setUp() {
        coordinates1 = new LinkedHashMap<String, Integer>();
        coordinates2 = new LinkedHashMap<String, Integer>();
        coordinates3 = new LinkedHashMap<String, Integer>();
        coordinates1.put(Maze.X_COORDINATES, 2);
        coordinates1.put(Maze.Y_COORDINATES, 2);
        coordinates2.put(Maze.X_COORDINATES, 2);
        coordinates2.put(Maze.Y_COORDINATES, 1);
        coordinates3.put(Maze.X_COORDINATES, 1);
        coordinates3.put(Maze.Y_COORDINATES, 1);
        coordinates = Arrays.asList(coordinates1, coordinates2, coordinates3);
    }
    
    @Test
    public void testConversion() {
        Path path = converter.convertJsonPath(coordinates);
        Assert.assertEquals(3, path.getCoordinates().size());
    }

}

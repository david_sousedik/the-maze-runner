package com.mag.mazerunner.domain;

import org.junit.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

public class MazeTest {
	
	private static String s;
	
	@BeforeClass
	public static void setUp() {
		s = "O O O O O X O\n" +
                "X X O X O O X\n" +
                "O X O O X X X\n" +
                "X X X O O X O\n" +
                "X X X X O O X\n" +
                "O O O O O O O\n" +
                "X X O X X X O";	
	}
	
	@Test
	public void analyseBoundsFromMazeString() {
		Maze maze = new Maze(s);
		maze.analyzeBounds();
		int width = maze.getWidth();
		int height = maze.getHeight();
		Assert.assertEquals(7, width);
		Assert.assertEquals(7, height);
		
	}
	
}

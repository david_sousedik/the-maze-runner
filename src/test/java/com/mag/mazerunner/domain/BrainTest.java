package com.mag.mazerunner.domain;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.mag.mazerunner.Brain;
import com.mag.mazerunner.service.MazeService;

@RunWith(MockitoJUnitRunner.class)
public class BrainTest {
	
	@Mock
	private MazeService mazeService;
	
	@Mock
	private BlockingQueue<Coordinate> requests;
	
	@Mock
	private BlockingQueue<MoveResult> replies;
	
	private static String s;
	
	private Path path;
	private Coordinate coordinate1;
	private Coordinate coordinate2;
	private Coordinate coordinate3;
	private Coordinate exit;
	
	private MoveResult result;
	private MoveResult finish;
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		s = "O O O\n" +
                    "X O X\n" +
		    "X O O";    
		
		path = new Path();
		coordinate1 = new Coordinate(1,2);
	        coordinate2 = new Coordinate(1,1);
	        coordinate3 = new Coordinate(0,1);
	        exit = new Coordinate(0, 0);
	        path.setCoordinates(Arrays.asList(coordinate1, coordinate2, coordinate3, exit));
	        
	        result = new MoveResult(coordinate1, Maze.WAY);
	        finish = new MoveResult(Maze.finalCoordinate, Maze.EXIT);
		
	}
	
	@Test
	public void mazeTest() throws InterruptedException {
		when(mazeService.getMaze()).thenReturn(s);
		when(mazeService.getPath()).thenReturn(path);
		when(replies.take()).thenReturn(result, finish);
		Brain brain = new Brain(requests, replies, mazeService);
		brain.run();
		verify(requests, times(2)).put(any(Coordinate.class));
	}

}

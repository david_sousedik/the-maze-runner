package com.mag.mazerunner.converter;

import java.util.LinkedHashMap;
import java.util.List;

import com.mag.mazerunner.domain.Coordinate;
import com.mag.mazerunner.domain.Maze;
import com.mag.mazerunner.domain.Path;

/**
 * Converting path getting from rest point to domain classes
 * 
 *
 */
public class JsonToPathConverter {

    public Path convertJsonPath(List<LinkedHashMap<String, Integer>> coordinates) {
        Path path = new Path();
        for (LinkedHashMap<String, Integer> step : coordinates) {
            Coordinate nextStep = new Coordinate(step.get(Maze.X_COORDINATES), step.get(Maze.Y_COORDINATES));
            path.getCoordinates().add(nextStep);
        }
        return path;
    }

}

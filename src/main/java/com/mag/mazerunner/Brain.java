package com.mag.mazerunner;

import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mag.mazerunner.domain.Coordinate;
import com.mag.mazerunner.domain.Maze;
import com.mag.mazerunner.domain.MoveResult;
import com.mag.mazerunner.domain.Path;
import com.mag.mazerunner.service.MazeService;

/**
 * Brain class represent guidance of maze to runner.
 * 
 * <p>
 * In particular has several tasks
 * 
 * <ul>
 * <li>get the maze map</li>
 * <li>calculate a possible path</li>
 * <li>send the direction to the next Runner's position</li>
 * </ul>
 * 
 */
@Component
public class Brain implements Runnable {

    @Autowired
    private MazeService mazeService;

    private Coordinate initialCoordinate;

    /** Sending next position, one-by-one */
    private BlockingQueue<Coordinate> requests;
    /** Acknowledging reply */
    private BlockingQueue<MoveResult> replies;

    public Brain(BlockingQueue<Coordinate> requests, BlockingQueue<MoveResult> replies, MazeService mazeService) {
        this.requests = requests;
        this.replies = replies;
        this.mazeService = mazeService;
    }

    @Override
    public void run() {
        System.out.println(this.getClass().getSimpleName() + ":" + "I am brain to solve the maze");
        doWork();

    }

    private void doWork() {
        // get maze from rest point
        System.out.println(this.getClass().getSimpleName() + ":" + "Loading map");
        Maze maze = new Maze(mazeService.getMaze());
        System.out.println(this.getClass().getSimpleName() + ":" + maze.toString());
        System.out.println(this.getClass().getSimpleName() + ":" + "Maze size is " + maze.getWidth() + " width and "
                + maze.getHeight() + " height");
        initialCoordinate = new Coordinate(maze.getWidth() - 1, maze.getHeight() - 1);
        System.out.println(this.getClass().getSimpleName() + ":" + "Starting from " + initialCoordinate.toString());

        // calculate the best way
        System.out.println(this.getClass().getSimpleName() + ":" + "Getting path from maze");
        Path path = mazeService.getPath();

        // iterate path
        for (Coordinate nextCordinate : path.getCoordinates()) {
            try {
                // instruct runner
                requests.put(nextCordinate);
                // get reply
                MoveResult result = replies.take();
                if (result.getPosition().equals(Maze.finalCoordinate)) {
                    System.out.println(this.getClass().getSimpleName() + ":" + "We found the way out");
                    return;
                } else if (result.getPosition().equals(nextCordinate)) {
                    System.out.println(this.getClass().getSimpleName() + ":" + "Reply acknowledged");
                    continue;
                }
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

}

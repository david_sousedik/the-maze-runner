package com.mag.mazerunner.service;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.ws.Response;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mag.mazerunner.converter.JsonToPathConverter;
import com.mag.mazerunner.domain.Coordinate;
import com.mag.mazerunner.domain.Path;

@Service
public class MazeService {
	
	private JsonToPathConverter converter = new JsonToPathConverter();
	
	public MazeService() {
		
	}

	public String getMaze() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType
				.parseMediaType("text/plain")));
		String maze = restTemplate.getForObject(
				"http://localhost:8080/rest/maze/map", String.class);
		return maze;
	}

	public Path getPath() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType
				.parseMediaType("application/json")));
		List<LinkedHashMap<String, Integer>> path = restTemplate.getForObject(
				"http://localhost:8080/rest/maze/path", List.class);
		return converter.convertJsonPath(path);
	}

	public void move(Coordinate coordinate) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType
				.parseMediaType("application/json")));
		restTemplate.postForEntity("http://localhost:8080/rest/maze/move",
				coordinate, Response.class);

	}

}

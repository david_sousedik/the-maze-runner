package com.mag.mazerunner;

import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mag.mazerunner.domain.Coordinate;
import com.mag.mazerunner.domain.Maze;
import com.mag.mazerunner.domain.MoveResult;
import com.mag.mazerunner.service.MazeService;

/**
 * Runner will find a way with Brains assistance.
 * 
 * <p>
 * In particular has several tasks
 * 
 * <ul>
 * <li>receive the new direction</li>
 * <li>move to the next position</li>
 * <li>replies back to the Brain</li>
 * </ul>
 * 
 */
@Component
public class Runner implements Runnable {

    @Autowired
    private MazeService mazeService;

    /** Receiving next position, one-by-one */
    private BlockingQueue<Coordinate> requests;
    /** Successfully reply */
    private BlockingQueue<MoveResult> replies;

    private boolean out = false;

    public Runner(BlockingQueue<Coordinate> requests, BlockingQueue<MoveResult> replies) {
        this.requests = requests;
        this.replies = replies;
        mazeService = new MazeService();
    }

    @Override
    public void run() {
        System.out.println(this.getClass().getSimpleName() + ":" + "I am runner stuck in maze");
        while (!out) {
            try {
                doWork();
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

    }

    private void doWork() {
        try {
            Coordinate coordinate = requests.take();
            System.out.println(this.getClass().getSimpleName() + ":" + "Direction recieved to + "
                    + coordinate.toString());
            mazeService.move(coordinate);
            MoveResult move = new MoveResult(new Coordinate(coordinate.getX(), coordinate.getY()), new Character(
                    Maze.WAY));
            System.out.println(this.getClass().getSimpleName() + ":" + "Reply sent");
            replies.put(move);
            if (coordinate.equals(Maze.finalCoordinate)) {
                out = true;
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

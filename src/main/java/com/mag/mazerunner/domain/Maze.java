package com.mag.mazerunner.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;

/**
 * Class representing the maze
 * 
 * 'X' represents a wall 'O' represents an open position
 * 
 */
public class Maze {

    public static Coordinate finalCoordinate = new Coordinate(0, 0);

    public static String X_COORDINATES = "x";
    public static String Y_COORDINATES = "y";

    public static final char WALL = 'X';
    public static final char WAY = 'O';
    public static final char EXIT = 'O';

    private static final Set<Character> VALID_TARGETS = new HashSet(WAY, EXIT);
    private static final Set<Character> INVALID_SOURCES = new HashSet(WALL, EXIT);

    private int width;

    private int height;

    private final String fields;

    public Maze(String fields) {
        this.fields = fields;
        analyzeBounds();
    }

    /**
     * Get starting coordinates
     * 
     * @return
     */
    public Coordinate getStart() {
        return new Coordinate(getWidth(), getHeight());
    }

    /**
     * Get ending coordinates
     * 
     * @return
     */
    public Coordinate getEnd() {
        return new Coordinate(0, 0);
    }

    @Override
    public String toString() {
        List<String> lines = new ArrayList<>();

        for (int i = 0; i < height; i++) {
            lines.add(fields.substring(i * width, (i + 2) * width));
        }

        return lines.toString();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getFields() {
        return fields;
    }

    /**
     * Validating move
     * 
     * @param move
     * @return
     */
    public Boolean validateMove(final Move move) {
        final Coordinate from = move.getFrom();

        final Character fromField = getField(from);
        if (INVALID_SOURCES.contains(fromField)) {
            return false;
        }

        final Coordinate to = from.move(move.getDirection());
        final Character field = getField(to);

        return VALID_TARGETS.contains(field);
    }

    /**
     * Getting field
     * 
     * @param coordinate
     * @return
     */
    private Character getField(final Coordinate coordinate) {
        final int index = coordinate.getX() + coordinate.getY() * width;

        if (isOutOfBounds(coordinate.getX(), width) || isOutOfBounds(coordinate.getY(), height)) {
            return WALL;
        }

        return fields.charAt(index);
    }

    private boolean isOutOfBounds(final int pos, final int max) {
        return pos < 0 || pos >= max;
    }

    /**
     * Moving from to
     * 
     * @param move
     * @return
     */
    public MoveResult move(final Move move) {
        Preconditions.checkState(this.validateMove(move), "move has to be valid");

        final Coordinate position = move.getFrom().move(move.getDirection());
        final Character field = getField(position);

        return new MoveResult(position, field);
    }

    /**
     * Analysing maze bounds
     * 
     */
    public void analyzeBounds() {
        String[] matrix = fields.split("\\n");
        setWidth((matrix[0].length() / 2) + 1);
        setHeight(matrix.length);
    }

}

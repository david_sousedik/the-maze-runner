package com.mag.mazerunner.domain;

import com.google.common.base.MoreObjects;

public class Move {

    private Coordinate from;

    private Direction direction;

    public Move() {
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("from", from).add("direction", direction).toString();
    }

    public void setFrom(final Coordinate from) {
        this.from = from;
    }

    public Coordinate getFrom() {
        return from;
    }

    public void setDirection(final Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

}

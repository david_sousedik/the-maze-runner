package com.mag.mazerunner.domain;


public enum Direction {
	WEST,
	NORTH,
	EAST,
	SOUTH;

}

package com.mag.mazerunner.domain;

import java.util.ArrayList;
import java.util.List;

public class Path {

    public List<Coordinate> coordinates = new ArrayList<Coordinate>();

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

}

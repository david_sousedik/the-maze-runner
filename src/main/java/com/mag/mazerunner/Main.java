package com.mag.mazerunner;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.springframework.boot.builder.SpringApplicationBuilder;

import com.mag.mazerunner.domain.Coordinate;
import com.mag.mazerunner.domain.MoveResult;
import com.mag.mazerunner.service.MazeService;

//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
public class Main {

    public static void main(String[] args) {
        new SpringApplicationBuilder().sources(Main.class).run(args);
        BlockingQueue<Coordinate> requests = new ArrayBlockingQueue<>(1);
        BlockingQueue<MoveResult> replies = new ArrayBlockingQueue<>(1);
        MazeService mazeService = new MazeService();
        Thread t1 = new Thread(new Brain(requests, replies, mazeService));
        Thread t2 = new Thread(new Runner(requests, replies));
        t1.start();
        t2.start();

    }

}
